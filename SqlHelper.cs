﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1
{
    class SqlHelper
    {
        private static readonly string connstr = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;

        /// <summary>
        /// 创建一个数据库连接
        /// </summary>
        /// <returns></returns>
        public static SqlConnection CreateConnection()
        {
            SqlConnection conn = new SqlConnection(connstr);
            conn.Open();
            return conn;
        }
        

        /// <summary>
        /// 执行不查询
        /// </summary>
        /// <param name="conn">连接数据库串</param>
        /// <param name="sql">sql语句</param>
        /// <param name="paramters">参数化参数</param>
        /// <returns>执行行数</returns>
        public static int ExecuteNonQuery(SqlConnection conn, string sql, params SqlParameter[] paramters)
        {
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Parameters.AddRange(paramters);
                return cmd.ExecuteNonQuery();
            }
        }

        public static int ExecuteNonQuery(SqlConnection conn, string sql,SqlTransaction tx, params SqlParameter[] paramters)
        {
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.Transaction = tx;
                cmd.CommandText = sql;
                cmd.Parameters.AddRange(paramters);
                return cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// 执行不查询
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="paramters">参数化参数</param>
        /// <returns>执行行数</returns>
        public static int ExecuteNonQuery(string sql, params SqlParameter[] paramters)
        {
            using (SqlConnection conn = CreateConnection())
            {
                return ExecuteNonQuery(conn, sql, paramters);
            }
        }

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="conn">连接数据库串</param>
        /// <param name="sql">sql语句</param>
        /// <param name="paramters">参数化参数</param>
        /// <returns>返回查询结果的第一行第一列的值</returns>
        public static object ExecuteScalar(SqlConnection conn, string sql, params SqlParameter[] paramters)
        {
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Parameters.AddRange(paramters);
                return cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="paramters">参数化参数</param>
        /// <returns>返回查询结果的第一行第一列的值</returns>
        public static object ExecuteScalar(string sql, params SqlParameter[] paramters)
        {
            using (SqlConnection conn = CreateConnection())
            {
                return ExecuteScalar(conn, sql, paramters);
            }
        }

        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="conn">连接数据库串</param>
        /// <param name="sql">sql语句</param>
        /// <param name="paramters">参数化参数</param>
        /// <returns>返回查询结果集</returns>
        public static DataTable ExecuteQuery(SqlConnection conn, string sql, params SqlParameter[] paramters)
        {
            DataTable dataTable = new DataTable();
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Parameters.AddRange(paramters);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dataTable.Load(reader);
                }
            }
            return dataTable;
        }

        public static DataTable ExecuteQuery(SqlConnection conn, string sql, SqlTransaction tx, params SqlParameter[] paramters)
        {
            DataTable dataTable = new DataTable();
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.Transaction = tx;
                cmd.CommandText = sql;
                cmd.Parameters.AddRange(paramters);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    dataTable.Load(reader);
                }
            }
            return dataTable;
        }


        /// <summary>
        /// 执行查询
        /// </summary>
        /// <param name="sql">sql语句</param>
        /// <param name="paramters">参数化参数</param>
        /// <returns>返回查询结果集</returns>
        public static DataTable ExecuteQuery(string sql, params SqlParameter[] paramters)
        {
            DataTable dataTable = new DataTable();
            using (SqlConnection conn = CreateConnection())
            {
                dataTable = ExecuteQuery(conn, sql, paramters);
            }
            return dataTable;
        }


    }
}
